#include<stdio.h>
int main()
{
    //A C program to check a given character is Vowel or Consonant.

    char letter;
    printf("Enter a character= ") ;    
    scanf("%c",&letter);
    
    if(letter=='a'||letter=='A'||letter=='e'||letter=='E'||letter=='i'||letter=='I'||letter=='o'||letter=='O'||letter=='u'||letter=='U')
        printf("%c is a Vowel",letter);
    else
        printf("%c is a Consonant",letter);
    return 0;

}
