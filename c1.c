#include<stdio.h>
int main()
{
    //A C program that takes a number from the user and checks whether that number is either positive or negative or zero.
    float x;
    printf("Enter a number=  \n");
    scanf("%f",&x);

    if(x>0)
        printf("%f is a positive number",x);
    else if(x<0)
        printf("%f is a negative number",x);
    else
        printf("%f is zero",x);
    return 0;
}
